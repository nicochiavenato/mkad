import requests

# Test if the distance is correct
def test_distance():
    address = 'Riga,Latvia'
    response = requests.get(f'http://localhost:5000/api/v1/distance/calc?address={address}')
    assert response.status_code == 200
    json = response.json()
    assert 'distanceMKAD' in json
    assert json['distanceMKAD'] == 828.1396494722815

# Test if returning the correct response if address inside MKAD
def test_distance_inside():
    address = 'Nagatino-Sadovniki'
    response = requests.get(f'http://localhost:5000/api/v1/distance/calc?address={address}')
    assert response.status_code == 200
    json = response.json()
    assert 'distanceMKAD' in json
    assert json['distanceMKAD'] == "The address is located inside the Moscow Ring Road."

# Test if empty address is returning status code 400
def test_distance_empty():
    address = ''
    response = requests.get(f'http://localhost:5000/api/v1/distance/calc?address={address}')
    assert response.status_code == 400
