# Introduction

This API was built in the micro-framework Flask, using Blueprint, for a test task. The objective is to find the distance from the Moscow Ring Road to a specified address. The address is passed to the application in a GET request, by URL parameter. If the specified address is located inside the MKAD, the distance isn't calculated. The result is also written on a 'results.log' file.

The address is converted to coordinates, for distance calculation, using the [Yandex Geocoder API](https://yandex.ru/dev/maps/geocoder/doc/desc/concepts/about.html).

## Request Example (GET)

A Yandex Geocoder developer key needs to be entered in the 'src\blueprints\distance.py' file.

The address parameter cannot be empty:

```http
GET http://localhost:5000/api/v1/distance/calc?address=Riga,Latvia
```

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `address` | `string` | **Required.** The address to measure distance from MKAD |

## Libraries and Test

This API also uses the libraries Python-Geojson, Geopy, and Turfpy for encoding coordinates and performing geospatial data analysis.

Pytest is used for the tests cases. A 'test_endpoints.py' file is included to test some cases: empty address, address inside and outside the Moscow Ring Road.

## Responses

The API returns a JSON response in the following format:

```javascript
{
  "distanceMKAD" : float or string
}
```

The `distanceMKAD` attribute contains a number representing the distance in kilometers of the given address for the closest point of the Moscow Ring Road. If the address is located inside the Moscow Ring Road instead `distanceMKAD` attribute contains a string ("The address is located inside the Moscow Ring Road.").

## Status Codes

This API returns the following status codes in its API:

| Status Code | Description |
| :--- | :--- |
| 200 | `OK` |
| 400 | `BAD REQUEST` |
| 404 | `NOT FOUND` |
| 500 | `INTERNAL SERVER ERROR` |
