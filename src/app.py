"""Flask Application"""

from flask import Flask
from src.blueprints.distance import distance

# Init Flask app
app = Flask(__name__)

# Register blueprint
app.register_blueprint(distance, url_prefix="/api/v1/distance")

